# Cocktail_Project

### Requirement
* PHP 7.2
* Mysql (phpmyadmin)

### Installation:

1. Install [Composer](https://getcomposer.org/)
2. Run `composer install` in `src/` directory
3. Create your `src/credentials/` directory:
    - Create `database-credentials.json` credentials file that have this structure:
    ```json
    {
        "db_name": "cocktail",
        "db_user": "<string>",
        "db_password": "<string>"
    }
    ```
4. Run `php src/install.php`
