<?php

abstract class Component
{
    abstract public function render(array $replacer = array());

    protected function rendered(string $pathToTemplate, array $replacer = array()): string
    {
        $template = file_get_contents($pathToTemplate);

        foreach ($replacer as $replacerKey => $replacerValue) {
            $template = str_replace($replacerKey, $replacerValue, $template);
        }

        return $template;
    }
}
