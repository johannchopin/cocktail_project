<?php

class DatabaseHandler
{
    protected $bdd;
    protected $dbName;
    protected $dbUser;
    protected $dbPassword;

    public function __construct(string $pathToCredentialsJson)
    {
        $credentialsContent = file_get_contents($pathToCredentialsJson);
        $credentials = json_decode($credentialsContent, TRUE);

        $this->dbName = $credentials["db_name"];
        $this->dbUser = $credentials["db_user"];
        $this->dbPassword = $credentials["db_password"];
    }

    public function connect(bool $withoutSpecificDb = FALSE)
    {
        try {
            if ($withoutSpecificDb) {
                $this->bdd = new PDO("mysql:host=localhost;charset=utf8", $this->dbUser, $this->dbPassword, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT => TRUE));
            } else {

                $this->bdd = new PDO("mysql:host=localhost;dbname={$this->dbName};charset=utf8", $this->dbUser, $this->dbPassword, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT => TRUE));
            }
        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

    public function disconnect()
    {
        $this->bdd = null;
    }

    public function query(string $sql, array $values = array())
    {
        $req = $this->bdd->prepare($sql);

        foreach ($values as $valueKey => &$value) {
            $req->bindValue(":" . $valueKey, $value);
        }

        $req->execute();
    }

    public function get(string $sqlRequest, array $values = array())
    {
        $statement = $this->bdd->prepare($sqlRequest);

        foreach ($values as $valueKey => &$value) {
            $statement->bindValue(":" . $valueKey, $value);
        }

        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function isPresent(string $sqlRequest, array $values = array())
    {
        $matching = $this->get($sqlRequest, $values);

        if (sizeof($matching) > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function getRequestString(string $sql, array $values = array()): string
    {
        foreach ($values as $valueKey => &$value) {
            $sql = str_replace(":" . $valueKey, $this->bdd->quote($value), $sql);
        }

        return $sql;
    }
};
