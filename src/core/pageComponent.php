<?php

require_once __DIR__ . '/databaseAccessComponent.php';
require_once __DIR__ . '/databaseHandler.php';
require_once __DIR__ . '/helper.php';

abstract class PageComponent extends DatabaseAccessComponent
{
    abstract public function render(array $replacer = array());

    public function __construct()
    {
        parent::__construct();
    }

    protected function uniqidReal($lenght = 8)
    {
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }

        return substr(bin2hex($bytes), 0, $lenght);
    }

    protected function getGeneratedToken()
    {
        $id = self::uniqidReal();

        for ($i = 0; $i < 3; $i++) {
            $id .= '-' . self::uniqidReal();
        }

        return $id;
    }

    protected function isUserConnected(): bool
    {
        return $this->database->isPresent("SELECT id FROM client WHERE token = :userToken", array(
            "userToken" => $this->getCookie("token"),
        ));
    }

    protected function setCookie(string $cookieName, string $value): void
    {
        setcookie("CP_" . $cookieName, $value, -1, '/');
    }

    protected function updateCookie(string $cookieName, string $value)
    {
        $cookieRealName = "CP_" . $cookieName;
        if (isset($_COOKIE[$cookieRealName])) {
            unset($_COOKIE[$cookieRealName]);
        }

        $this->setcookie($cookieName, $value);
    }

    protected function getCookie(string $cookieName)
    {
        if (isset($_COOKIE["CP_" . $cookieName])) {
            return $_COOKIE["CP_" . $cookieName];
        }

        return NULL;
    }

    public function getUserToken()
    {
        return $this->getCookie("token");
    }

    public function getUserId(): string
    {
        $userIdResponse = $this->database->get("SELECT id FROM client WHERE token = :token LIMIT 1", array(
            "token" => $this->getUserToken(),
        ));

        return $userIdResponse[0]["id"];
    }

    public function linkRender(string $link, string $linkContent = ""): string
    {
        $linkTemplate = '<a href="__LINK__">__CONTENT__</a>';

        return str_replace("__CONTENT__", $linkContent, str_replace("__LINK__", $link, $linkTemplate));
    }

    public function listRender(array $elements): string
    {
        $listRender = "";

        foreach ($elements as $element) {
            $listRender .= "<li>{$element}</li>";
        }

        return $listRender;
    }

    public function getFavouriteRecipesFromCookie(): array
    {
        $favouriteRecipesIdsCookie = $this->getCookie("favourite_recipes");

        if (isset($favouriteRecipesIdsCookie)) {
            return json_decode($favouriteRecipesIdsCookie);
        } else {
            return array();
        }
    }
}
