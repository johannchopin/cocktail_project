<?php

require_once __DIR__ . '/../../../core/databaseAccessComponent.php';

class Recipe extends DatabaseAccessComponent
{
    protected $recipeId;
    protected $recipe;
    protected $isFavourite;
    protected $isUserConnected;

    public function __construct(string $recipeId, bool $isFavourite, bool $isUserConnected)
    {
        parent::__construct();

        $this->recipeId = $recipeId;
        $this->recipe = $this->getRecipe();
        $this->isFavourite = $isFavourite;
        $this->isUserConnected = $isUserConnected;
    }

    public function render(array $replacer = array())
    {
        return $this->rendered(__DIR__ . '/recipe.html', array(
            "__ID__"            => $this->recipeId,
            "__IS_CONNECTED__"  => $this->isUserConnected ? "true" : "false",
            "__NAME__"          => $this->recipe["titre"],
            "__IS_FAVOURITE__"  => $this->isFavourite ? "favourite" : "",
            "__IMG_PATH__"      => $this->getPathToImgFromRecipeName($this->recipe["titre"]),
            "__INGREDIENTS__"   => $this->ingredientsRender($this->recipe["ingredients"]),
            "__PREPARATION__"   => $this->recipe["preparation"],
            "__TAGS__"          => $this->ingredientsTagRender($this->recipe["titre"]),
        ));
    }

    protected function getRecipe(): array
    {
        $cocktailResponse = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette WHERE id = :id", array(
            "id" => $this->recipeId,
        ));

        return $cocktailResponse[0];
    }

    protected function getIngredientsTag(string $recipeName)
    {
        return $this->database->get(
            "SELECT nom FROM ingredient WHERE 
            id IN (SELECT id_ingredient FROM est_compose WHERE 
            id_recette = (SELECT id FROM recette WHERE titre = :recipeName))",
            array(
                "recipeName" => $recipeName,
            )
        );
    }

    protected function ingredientsTagRender(string $recipeName): string
    {
        $render = "";
        $recipeIngredientsTag = $this->getIngredientsTag($recipeName);

        foreach ($recipeIngredientsTag as $ingredientName) {
            $ingredientIdResponse = $this->database->get(
                "SELECT id FROM ingredient WHERE nom = :ingredientName ",
                array(
                    "ingredientName" => $ingredientName["nom"],
                )
            );
            $ingredientId = $ingredientIdResponse[0]["id"];

            $badgeRender = $this->rendered(__DIR__ . '/../badge.html', array(
                "__BADGE_TYPE__" => "dark",
                "__CONTENT__"    => $ingredientName["nom"],
            ));

            $render .= "<a href='?recipe_contain=[{$ingredientId}]'>{$badgeRender}</a>";
        }

        return $render;
    }

    protected function getPathToImgFromRecipeName(string $cocktailName): string
    {
        $cocktailNameWithoutSpaces = str_replace(" ", "_", $cocktailName);
        $cocktailNameLowerCase = strtolower($cocktailNameWithoutSpaces);
        $imgName = ucfirst($cocktailNameLowerCase) . ".jpg";
        $pathToImg = "./assets/images/cocktails/" . $imgName;

        if (in_array($imgName, scandir(__DIR__ . "/../../../assets/images/cocktails/"))) {
            return $pathToImg;
        }

        return "./assets/images/noImg.png";
    }

    protected function ingredientsRender(string $ingredients): string
    {
        return $this->listRender(explode("|", $ingredients));
    }

    protected function listRender(array $elements): string
    {
        $listRender = "";

        foreach ($elements as $element) {
            $listRender .= "<li>{$element}</li>";
        }

        return $listRender;
    }
}
