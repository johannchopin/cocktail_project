<?php

require_once __DIR__ . '/../../../core/databaseAccessComponent.php';

class Navbar extends DatabaseAccessComponent
{
    public function __construct()
    {
        parent::__construct();

        if (isset($_POST["search_request"])) {
            $this->handleSearchRequest();
        }
    }

    public function render(array $replacer = array())
    {
        return $this->rendered(__DIR__ . '/navbar.html', $replacer);
    }

    protected function handleSearchRequest(): void
    {
        $searchRequest = $_POST["search_request"] . " ";

        $filteredIngredients = $this->getFilteredIngredients($searchRequest);
        $filteredIngredientsId = $this->getFilteredIngredientsId($filteredIngredients);

        $this->redirectWithSearchRequest($filteredIngredientsId["toHave"], $filteredIngredientsId["toNotHave"]);
    }

    protected function getFilteredIngredientsId(array $filteredIngredients): array
    {
        return array(
            "toHave"    => $this->getIngredientsId($filteredIngredients["toHave"]),
            "toNotHave" => $this->getIngredientsId($filteredIngredients["toNotHave"]),
        );
    }

    protected function redirectWithSearchRequest(array $ingredientIdToHave, array $ingredientIdToNotHave)
    {
        $ingredientIdToHaveAsJson = json_encode($ingredientIdToHave);
        $ingredientIdToNotHaveAsJson = json_encode($ingredientIdToNotHave);

        $searchRequestAsUrl = '';

        if (!empty($ingredientIdToHave)) {
            $searchRequestAsUrl .= "&recipe_contain={$ingredientIdToHaveAsJson}";
        }

        if (!empty($ingredientIdToNotHave)) {
            $searchRequestAsUrl .= "&recipe_not_contain={$ingredientIdToNotHaveAsJson}";
        }

        header("Location: ./?p=home" . $searchRequestAsUrl, TRUE, 302);
    }

    protected function getFilteredIngredients(string $searchRequest): array
    {
        $elementsInQuotesPattern = '/"(.*?)"/';
        $elementsInQuotesWithPlusPattern = '/\+"(.*?)"/';
        $elementsInQuotesWithMinusPattern = '/\-"(.*?)"/';
        $elementsStartWithPlusPattern = '/\+(\w+)/';
        $elementsStartWithMinusPattern = '/\-(\w+)/';

        // ---
        preg_match_all($elementsInQuotesWithPlusPattern, $searchRequest, $elementsInQuotesWithPlusResult);
        preg_match_all($elementsStartWithPlusPattern, $searchRequest, $elementsStartWithPlusResult);
        $elementsInQuotesWithPlus = $elementsInQuotesWithPlusResult[1];
        $elementsStartWithPlus = $elementsStartWithPlusResult[1];

        $ingredientsWithPlus = array_merge($elementsInQuotesWithPlus, $elementsStartWithPlus);
        $sanitizedIngredientsWithPlus = array_unique($ingredientsWithPlus, SORT_REGULAR);

        $searchRequest = Helper::removeElmtsInString($searchRequest, array_merge($elementsInQuotesWithPlusResult[0], $elementsStartWithPlusResult[0]));
        // ---

        // ---
        preg_match_all($elementsInQuotesWithMinusPattern, $searchRequest, $elementsInQuotesWithMinusResult);
        preg_match_all($elementsStartWithMinusPattern, $searchRequest, $elementsStartWithMinusResult);
        $elementsInQuotesWithMinus = $elementsInQuotesWithMinusResult[1];
        $elementsStartWithMinus = $elementsStartWithMinusResult[1];

        $ingredientsWithMinus = array_merge($elementsInQuotesWithMinus, $elementsStartWithMinus);
        $sanitizedIngredientsWithMinus = array_unique($ingredientsWithMinus, SORT_REGULAR);

        $searchRequest = Helper::removeElmtsInString($searchRequest, array_merge($elementsInQuotesWithMinusResult[0], $elementsStartWithMinusResult[0]));
        // ---

        // ---
        preg_match_all($elementsInQuotesPattern, $searchRequest, $elementsInQuotesResult);
        $elementsInQuotes = $elementsInQuotesResult[1];

        $searchRequest = Helper::removeElmtsInString($searchRequest, $elementsInQuotesResult[0]);
        // ---

        $ingredientsToHave = array_unique(array_merge($sanitizedIngredientsWithPlus, $elementsInQuotes, explode(" ", $searchRequest)));
        $ingredientsToNotHave = $sanitizedIngredientsWithMinus;

        return array(
            "toHave"    => $ingredientsToHave,
            "toNotHave" => $ingredientsToNotHave,
        );
    }

    protected function getIngredientsId(array $ingredientsName): array
    {
        $ingredientsId = array();

        foreach ($ingredientsName as $ingredientName) {
            array_push($ingredientsId, $this->getIngredientId($ingredientName));
        }

        return array_filter($ingredientsId, function ($var) {
            return !is_null($var);
        });
    }

    protected function getIngredientId(string $ingredientName)
    {
        $ingredientIdResponse = $this->database->get(
            "SELECT id FROM ingredient WHERE nom = :ingredientName ",
            array(
                "ingredientName" => $ingredientName,
            )
        );

        if (isset($ingredientIdResponse[0]["id"])) {
            return $ingredientIdResponse[0]["id"];
        }

        return NULL;
    }
}
