<?php

class Helper
{

    public static function getTimestamp()
    {
        return date_timestamp_get(date_create());
    }

    public static function replaceValuesInString(string $string, array $values)
    {
        foreach ($values as $key => $value) {
            $string = str_replace($key, $value, $string);
        }

        return $string;
    }

    public static function sanitizeStringInArray(array $array)
    {
        foreach ($array as $key => $value) {
            $array[$key] = htmlspecialchars($value);
        }

        return $array;
    }

    protected static function uniqidReal($lenght = 8)
    {
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }

        return substr(bin2hex($bytes), 0, $lenght);
    }

    public static function getId()
    {
        $id = self::uniqidReal();

        for ($i = 0; $i < 3; $i++) {
            $id .= '-' . self::uniqidReal();
        }

        return $id;
    }

    public static function getJsonFromFile(string $filePath)
    {
        $str = file_get_contents($filePath);

        return json_decode($str, TRUE);
    }

    public static function redirectToLink(string $link)
    {
        $encodedUrl = urlencode($link);
        header("Location: " . $encodedUrl);
        die();
    }

    public static function isJson(string $string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public static function removeElmtsInString(string $string, array $elmts): string
    {
        $finalString = $string;

        foreach ($elmts as $value) {
            $finalString = str_replace($value, "", $finalString);
        }

        return $finalString;
    }

    public static function issetInArrayOrElse($arr, $key, $elseVal=""){
        if(array_key_exists($key, $arr))
            return $arr[$key];
        return $elseVal;
    }
}
