<?php

require_once __DIR__ . '/component.php';
require_once __DIR__ . '/databaseHandler.php';

abstract class DatabaseAccessComponent extends Component
{
    abstract public function render(array $replacer = array());

    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseHandler(__DIR__ . '/../credentials/database-credentials.json');
        $this->database->connect();
    }
}
