<?php

require_once __DIR__ . '/../databaseHandler.php';

class FavouriteRecipe
{
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseHandler(__DIR__ . '/../../credentials/database-credentials.json');

        $this->database->connect();
        $this->init();
        $this->database->disconnect();
    }

    protected function init(): void
    {
        if (isset($_POST["id"]) && isset($_POST["method"]) && isset($_POST["user_token"])) {
            $userToken = htmlspecialchars($_POST["user_token"]);
            $recipeId = htmlspecialchars($_POST["id"]);
            $method = htmlspecialchars($_POST["method"]);

            $userId = $this->getUserIdFromToken($userToken);

            switch ($method) {
                case "ADD":
                    $this->addFavouriteRecipe($recipeId, $userId);
                    break;

                case "DEL":
                    $this->deleteFavouriteRecipe($recipeId, $userId);
                    break;
            }

            echo json_encode(array(
                "success" => TRUE,
            ));
        } else {
            echo json_encode(array(
                "error" => "Invalid given arguments",
            ));
        }
    }

    protected function getUserIdFromToken(string $token): string
    {
        $userIdResponse = $this->database->get("SELECT id FROM client WHERE token = :token LIMIT 1", array(
            "token" => $token
        ));

        return $userIdResponse[0]["id"];
    }

    protected function addFavouriteRecipe(string $recipeId, string $userId)
    {
        $this->database->query("INSERT INTO a_pour_recette_favorite(id_client, id_recette) VALUES(:userId, :recipeId)", array(
            "userId"   => $userId,
            "recipeId" => $recipeId,
        ));
    }

    protected function deleteFavouriteRecipe(string $recipeId, string $userId)
    {
        $this->database->query("DELETE FROM a_pour_recette_favorite WHERE id_client = :userId AND id_recette = :recipeId", array(
            "userId"   => $userId,
            "recipeId" => $recipeId,
        ));
    }
}

new FavouriteRecipe();
