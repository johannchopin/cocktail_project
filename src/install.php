<?php

require_once __DIR__ . '/Donnees.inc.php';

require_once __DIR__ . '/core/databaseHandler.php';

class Install
{
    protected $pathToDbStructure;
    protected $database;
    protected $finalRequest;
    protected $recipes;
    protected $ingredients;

    public function __construct(string $pathToDbStructure, array $recipes, array $ingredients)
    {
        $this->database = new DatabaseHandler(__DIR__ . '/credentials/database-credentials.json');
        $this->pathToDbStructure = $pathToDbStructure;
        $this->recipes = $recipes;
        $this->ingredients = $ingredients;

        $this->init();
    }

    protected function init(): void
    {
        $this->database->connect(TRUE);

        $this->initDatabaseStructure();
        $this->initRecipeTable();
        $this->initIngredientTable();
        $this->initEst_composeTable();
        $this->initA_pour_super_categorieTable();

        $this->database->query($this->finalRequest);
        $this->database->disconnect();
    }

    protected function initDatabaseStructure(): void
    {
        $databaseStructure = $this->getDatabaseStructure();

        $this->database->query($databaseStructure);
    }

    protected function getDatabaseStructure(): string
    {
        return file_get_contents($this->pathToDbStructure);
    }

    protected function initRecipeTable(): void
    {
        foreach ($this->recipes as $recipe) {
            $this->finalRequest .= $this->database->getRequestString("INSERT INTO recette(titre, ingredients, preparation) VALUES(:title, :ingredients, :preparation);", array(
                "title" => $recipe["titre"],
                "ingredients" => $recipe["ingredients"],
                "preparation" => $recipe["preparation"],
            ));
        }
    }

    protected function initIngredientTable(): void
    {
        foreach ($this->ingredients as $ingredientName => $ingredient) {
            $this->finalRequest .= $this->database->getRequestString("INSERT INTO ingredient(nom) VALUES(:nom);", array(
                "nom" => $ingredientName,
            ));
        }
    }

    protected function initEst_composeTable(): void
    {
        foreach ($this->recipes as $recipe) {
            foreach ($recipe["index"] as $ingredientName) {
                $this->finalRequest .= $this->database->getRequestString(
                    "INSERT INTO est_compose(id_recette, id_ingredient) VALUES(
                    (SELECT id FROM recette WHERE titre = :recipeTitle), 
                    (SELECT id FROM ingredient WHERE nom = :ingredientName));",
                    array(
                        "recipeTitle"    => $recipe["titre"],
                        "ingredientName" => $ingredientName,
                    )
                );
            }
        }
    }

    protected function initA_pour_super_categorieTable(): void
    {
        foreach ($this->ingredients as $ingredientName => $ingredient) {
            if (isset($ingredient["super-categorie"])) {
                foreach ($ingredient["super-categorie"] as $superCategoryName) {
                    $this->finalRequest .= $this->database->getRequestString(
                        "INSERT INTO a_pour_super_categorie(id_ingredient, id_ingredient_parent) VALUES(
                        (SELECT id FROM ingredient WHERE nom = :ingredientName),
                        (SELECT id FROM ingredient WHERE nom = :parentIngredientName));",
                        array(
                            "ingredientName"       => $ingredientName,
                            "parentIngredientName" => $superCategoryName,
                        )
                    );
                }
            }
        }
    }
}

new Install(__DIR__ . '/database.sql', $Recettes, $Hierarchie);
