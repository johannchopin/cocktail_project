<?php

// DISPLAY PHP ERRORS
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);


// INIT COMPONENTS ZONE
require_once __DIR__ . '/core/pageComponent.php';
// INIT COMPONENTS ZONE

// INIT COMPONENTS ZONE
require_once __DIR__ . '/core/components/navbar/navbar.php';
// INIT COMPONENTS ZONE

class Index extends PageComponent
{
    protected $selectedPage;
    protected $selectedPageComponent;

    public function __construct()
    {
        parent::__construct();

        $this->setSelectedPage();
        $this->setSelectedPageComponent();
        $this->render();
    }

    public function render(array $replacer = array())
    {
        echo $this->rendered(__DIR__ . "/template.html", array(
            "__SPECIFIC_STYLES__"   => $this->getPathToSpecificStyles(),
            "__SPECIFIC_SCRIPT__"   => $this->getPathToSpecificScripts(),
            "__NAVBAR__"            => $this->navbarRender(),
            "__PAGE__"              => $this->selectedPageComponent->render(),
        ));
    }

    protected function setSelectedPage(): void
    {
        if (isset($_GET["p"])) {
            $this->selectedPage = $_GET["p"];
        } else {
            $this->selectedPage = "home";
        }
    }

    protected function setSelectedPageComponent(): void
    {
        switch ($this->selectedPage) {
            default:
                require_once __DIR__ . '/pages/home/index.php';
                $this->selectedPageComponent = new HomePage();
                break;

            case "home":
                require_once __DIR__ . '/pages/home/index.php';
                $this->selectedPageComponent = new HomePage();
                break;

            case "cocktail":
                require_once __DIR__ . '/pages/cocktail/index.php';
                $this->selectedPageComponent = new CocktailPage();
                break;

            case "login":
                require_once __DIR__ . '/pages/login/index.php';
                $this->selectedPageComponent = new LoginPage();
                break;

            case "register":
                require_once __DIR__ . '/pages/register/index.php';
                $this->selectedPageComponent = new RegisterPage();
                break;

            case "userSettings":
                require_once __DIR__ . '/pages/userSettings/index.php';
                $this->selectedPageComponent = new UserSettingsPage();
                break;

            case "favouriteRecipes":
                require_once __DIR__ . '/pages/favouriteRecipes/index.php';
                $this->selectedPageComponent = new FavouriteRecipesPage();
                break;
        }
    }

    protected function getPathToSpecificStyles(): string
    {
        return "./pages/{$this->selectedPage}/styles.css";
    }

    protected function getPathToSpecificScripts(): string
    {
        return "./pages/{$this->selectedPage}/script.js";
    }

    protected function navbarRender(): string
    {
        return (new Navbar())->render(array(
            "__LINK_TO_REGISTER_PAGE__" => "?p=register",
            "__LINK_TO_LOGIN_PAGE__" => "?p=login",
            "__LINK_TO_USER_SETTINGS_PAGE__" => "?p=userSettings",
            "__USER_FEATURE_DISPLAY__" => $this->isUserConnected() ? 'inline-block' : 'none',
            "__SEARCHBAR_DISPLAY__"  => $this->isUserConnected() ? 'visible' : 'hidden',
        ));
    }

    protected function sidebarRender()
    {
        return $this->rendered(__DIR__ . '/core/components/sidebar.html');
    }

    protected function getAllIngredientsHasJsList(): string
    {
        $allIngredients = array();
        $ingredientsResponse = $this->database->get("SELECT nom FROM ingredient");

        foreach ($ingredientsResponse as $ingredient) {
            array_push($allIngredients, $ingredient['nom']);
        }

        return json_encode($allIngredients);
    }
}


$index = new Index();
