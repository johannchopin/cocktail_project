<?php

require_once __DIR__ . '/../../core/helper.php';

// INIT COMPONENTS ZONE
require_once __DIR__ . '/../../core/pageComponent.php';
// INIT COMPONENTS ZONE

class RegisterPage extends PageComponent
{
    protected $hasPageErrors = FALSE;
    protected $errorMessage = "";
    protected $successMessage = "Successfully registered";

    public function __construct()
    {
        parent::__construct();

        if (isset($_POST['submit'])) {
            $this->createUser();
        }
    }

    public function render(array $replacer = array()): string
    {
        // Handle Gender completion
        $genderMale = "";
        $genderFemale = "";
        if(isset($_POST["user_gender"])){
            if ($_POST["user_gender"] == "m")
                $genderMale = "checked";
            else
                $genderFemale = "checked";
        }

        return $this->rendered(__DIR__ . "/template.html", array(
            "__STATUS_MODAL__"        => $this->errorModalRender(),
            "__INPUT_LOGIN__"         => Helper::issetInArrayOrElse($_POST,"user_login"),
            "__INPUT_PWD__"           => Helper::issetInArrayOrElse($_POST, "user_pwd"),
            "__INPUT_NAME__"          => Helper::issetInArrayOrElse($_POST,"user_name"),
            "__INPUT_FIRSTNAME__"     => Helper::issetInArrayOrElse($_POST,"user_firstname"),
            "__INPUT_GENDER_MALE__"   => $genderMale,
            "__INPUT_GENDER_FEMALE__" => $genderFemale,
            "__INPUT_EMAIL__"         => Helper::issetInArrayOrElse($_POST,"user_mail"),
            "__INPUT_BIRTHDATE__"     => Helper::issetInArrayOrElse($_POST,"user_birth_date"),
            "__INPUT_ADRESS__"        => Helper::issetInArrayOrElse($_POST,"user_adress"),
            "__INPUT_CODE__"          => Helper::issetInArrayOrElse($_POST,"user_code"),
            "__INPUT_CITY__"          => Helper::issetInArrayOrElse($_POST,"user_city"),
            "__INPUT_PHONENUMBER__"   => Helper::issetInArrayOrElse($_POST,"user_phone_number"),
        ));
    }

    protected function createUser()
    {
        if ($this->checkUserDataExistence()) {
            $userToken = $this->getGeneratedToken();

            if ($this->isLoginUnique($_POST["user_login"])) {
                $this->setCookie("token", $userToken);
                $this->insertUserToDb($userToken);

                header("Location: ./?p=home");
            } else {
                $this->hasPageErrors = TRUE;
                $this->errorMessage = "the login is already used !";
            }
        } else {
            $this->hasPageErrors = TRUE;
            $this->errorMessage = "Please enter all required field !";
        }
    }

    protected function isLoginUnique(string $userLogin): bool
    {
        return !$this->database->isPresent("SELECT * FROM client WHERE login = :userLogin", array(
            "userLogin" => $userLogin,
        ));
    }

    protected function checkUserDataExistence(): bool
    {
        return isset($_POST["user_login"]) && isset($_POST["user_pwd"]) && isset($_POST["user_name"]) && isset($_POST["user_firstname"])
            && isset($_POST["user_mail"]) && isset($_POST["user_gender"]) && isset($_POST["user_adress"]) && isset($_POST["user_code"])
            && isset($_POST["user_city"]) &&  trim($_POST["user_login"]) !== "" && trim($_POST["user_pwd"]) !== "";
    }

    protected function concUserAdress(string $adress, string $postal, string $city)
    {
        return $adress . "|" . $postal . "|" . $city;
    }

    protected function insertUserToDb(string $token): void
    {
        $this->database->query("INSERT INTO client(token, login, password, name, first_name, gender, mail, birth_date, adress, phone_number) VALUES(:token, :login, :password, :name, :first_name, :gender, :mail, :birth_date, :adress, :phone_number)", array(
            "token"      => $token,
            "login"      => $_POST["user_login"],
            "password"   => $_POST["user_pwd"],
            "name"       => $_POST["user_name"],
            "first_name" => $_POST["user_firstname"],
            "gender"     => $_POST["user_gender"],
            "mail"       => $_POST["user_mail"],
            "birth_date" => $_POST["user_birth_date"],
            "adress"     => $this->concUserAdress($_POST["user_adress"], $_POST["user_code"], $_POST["user_city"]),
            "phone_number" => $_POST["user_phone_number"],
        ));
    }

    public function errorModalRender(): string
    {
        if ($this->hasPageErrors) {
            return $this->rendered(__DIR__ . "/../../core/components/errorModal.html", array(
                "__MESSAGE__" => $this->errorMessage,
            ));
        }

        if ($this->checkUserDataExistence()) {
            return $this->rendered(__DIR__ . "/../../core/components/successModal.html", array(
                "__MESSAGE__" => $this->successMessage,
            ));
        }

        return "";
    }
}
