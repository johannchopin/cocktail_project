<?php

require_once __DIR__ . '/../../core/helper.php';

// INIT COMPONENTS ZONE
require_once __DIR__ . '/../../core/pageComponent.php';
// INIT COMPONENTS ZONE

class LoginPage extends PageComponent
{
    protected $hasPageErrors = FALSE;
    protected $errorMessage = "";
    protected $successMessage = "Successfully registered";

    public function __construct()
    {
        parent::__construct();

        if (isset($_POST['submit'])) {
            $this->logUser();
        }
    }

    public function render(array $replacer = array()): string
    {
        return $this->rendered(__DIR__ . "/template.html", array(
            "__STATUS_MODAL__"     => $this->errorModalRender(),
            "__INPUT_LOGIN__"      => Helper::issetInArrayOrElse($_POST,"user_login"),
            "__INPUT_PWD__"        => Helper::issetInArrayOrElse($_POST,"user_pwd"),
        ));
    }

    protected function logUser(): void
    {
        if ($this->checkUserDataExistence()) {
            if ($this->isUser()) {
                $userData = $this->getUserData();
                $this->setCookie("token", $userData["token"]);
                header("Location: ./?p=home");
            } else {
                $this->hasPageErrors = TRUE;
                $this->errorMessage = "Not user";
            }
        } else {
            $this->hasPageErrors = TRUE;
            $this->errorMessage = "Please enter all required field !";
        }
    }

    protected function checkUserDataExistence(): bool
    {
        return isset($_POST["user_login"]) && isset($_POST["user_pwd"]) &&  trim($_POST["user_login"]) !== "" && trim($_POST["user_pwd"]) !== "";
    }

    protected function isUser(): bool
    {
        return $this->database->isPresent("SELECT * FROM client WHERE login = :userLogin AND password = :userPwd", array(
            "userLogin" => $_POST["user_login"],
            "userPwd"   => $_POST["user_pwd"],
        ));
    }

    protected function getUserData(): array
    {
        $userTokenResponse = $this->database->get("SELECT token, name, first_name FROM client WHERE login = :userLogin AND password = :userPwd", array(
            "userLogin" => $_POST["user_login"],
            "userPwd"   => $_POST["user_pwd"],
        ));

        return array(
            "name"       => $userTokenResponse[0]["name"],
            "firstName"  => $userTokenResponse[0]["first_name"],
            "token"      => $userTokenResponse[0]["token"],
        );
    }

    public function errorModalRender(): string
    {
        if ($this->hasPageErrors) {
            return $this->rendered(__DIR__ . "/../../core/components/errorModal.html", array(
                "__MESSAGE__" => $this->errorMessage,
            ));
        }

        if ($this->checkUserDataExistence()) {
            return $this->rendered(__DIR__ . "/../../core/components/successModal.html", array(
                "__MESSAGE__" => $this->successMessage,
            ));
        }

        return "";
    }
}
