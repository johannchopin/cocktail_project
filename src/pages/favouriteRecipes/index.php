<?php

// INIT COMPONENTS ZONE
require_once __DIR__ . '/../../core/pageComponent.php';
require_once __DIR__ . '/../../core/components/recipe/recipe.php';
// INIT COMPONENTS ZONE

class FavouriteRecipesPage extends PageComponent
{
    protected $favouriteRecipesId;

    public function __construct()
    {
        parent::__construct();

        $this->setFavouriteRecipes();
    }

    protected function setFavouriteRecipes()
    {
        $this->favouriteRecipesId = $this->getFavouriteRecipesFromCookie();
    }

    public function render(array $replacer = array()): string
    {
        return $this->rendered(__DIR__ . "/template.html", array(
            "__RECIPES__" => $this->recipesRender(),
        ));
    }

    protected function recipesRender(): string
    {
        $recipesRender = '';

        foreach ($this->favouriteRecipesId as $favouriteRecipeId) {
            $recipesRender .= (new Recipe($favouriteRecipeId, TRUE, $this->isUserConnected()))->render();
        }

        return $recipesRender;
    }
}
