<?php

// INIT COMPONENTS ZONE
require_once __DIR__ . '/../../core/pageComponent.php';
require_once __DIR__ . '/../../core/components/recipe/recipe.php';
// INIT COMPONENTS ZONE

class CocktailPage extends PageComponent
{
    protected $cocktailId;

    public function __construct()
    {
        parent::__construct();

        if (isset($_GET["id"])) {
            $this->cocktailId = $_GET["id"];
        } else {
            $this->cocktailId = 1;
        }
    }

    public function render(array $replacer = array()): string
    {
        return $this->rendered(__DIR__ . "/template.html", array(
            "__RECIPE__" => $this->cocktailRender(),
        ));
    }

    protected function cocktailRender(): string
    {
        return (new Recipe($this->cocktailId, $this->isRecipeFavourite(), $this->isUserConnected()))->render();
    }

    protected function isRecipeFavourite(): bool
    {
        return in_array($this->cocktailId, $this->getFavouritesRecipesId());
    }

    protected function getFavouritesRecipesId(): array
    {
        $favouriteRecipesIdsCookie = $this->getCookie("favourite_recipes");

        if (isset($favouriteRecipesIdsCookie)) {
            return json_decode($favouriteRecipesIdsCookie);
        } else {
            return array();
        }
    }
}
