<?php

// INIT COMPONENTS ZONE
require_once __DIR__ . '/../../core/pageComponent.php';
// INIT COMPONENTS ZONE

class UserSettingsPage extends PageComponent
{
    protected $hasPageErrors = FALSE;
    protected $errorMessage = "";

    protected $hasPageSuccess = FALSE;
    protected $successMessage = "";

    public function __construct()
    {
        parent::__construct();

        if (isset($_POST['submit'])) {
            $this->saveUserSettingsChanges();
        }
    }

    public function render(array $replacer = array()): string
    {
        $userData = $this->getUserData();

        return $this->rendered(__DIR__ . "/template.html", array(
            "__ERROR_MODAL__"     => $this->errorModalRender(),
            "__SUCCESS_MODAL__"   => $this->successModalRender(),
            "__USER_LOGIN__"      => $userData["login"],
            "__USER_PWD__"        => $userData["pwd"],
            "__USER_NAME__"       => $userData["name"],
            "__USER_FIRST_NAME__" => $userData["firstName"],
        ));
    }

    protected function saveUserSettingsChanges(): void
    {
        if ($this->checkUserDataExistence()) {
            try {
                $this->database->query(
                    "UPDATE client SET login = :login, password = :password, name = :name, first_name = :firstName WHERE token = :userToken",
                    array(
                        "login"     => $_POST["user_login"],
                        "password"  => $_POST["user_pwd"],
                        "name"      => $_POST["user_name"],
                        "firstName" => $_POST["user_firstname"],
                        "userToken" => $this->getUserToken(),
                    )
                );

                $this->hasPageSuccess = TRUE;
                $this->successMessage = "Saved !";
                header("Location: ./?p=home");
            } catch (Exception $e) {
                $this->hasPageErrors = TRUE;
                $this->errorMessage = "Something went wrong !";
            }
        } else {
            $this->hasPageErrors = TRUE;
            $this->errorMessage = "Please enter all required field !";
        }
    }

    protected function getUserData(): array
    {
        $userDataResponse = $this->database->get("SELECT login, password, name, first_name FROM client WHERE token = :userToken", array(
            "userToken" => $this->getUserToken(),
        ));

        return array(
            "name"      => $userDataResponse[0]["name"],
            "firstName" => $userDataResponse[0]["first_name"],
            "login"     => $userDataResponse[0]["login"],
            "pwd"       => $userDataResponse[0]["password"],
        );
    }

    protected function checkUserDataExistence(): bool
    {
        return isset($_POST["user_login"]) && isset($_POST["user_pwd"]) && isset($_POST["user_name"]) && isset($_POST["user_firstname"])
            &&  trim($_POST["user_login"]) !== "" && trim($_POST["user_pwd"]) !== "" && trim($_POST["user_name"]) !== "" && trim($_POST["user_firstname"]) !== "";
    }

    protected function errorModalRender(): string
    {
        if ($this->hasPageErrors) {
            return $this->rendered(__DIR__ . "/../../core/components/errorModal.html", array(
                "__MESSAGE__" => $this->errorMessage,
            ));
        }

        return "";
    }

    protected function successModalRender(): string
    {
        if ($this->hasPageSuccess) {
            return $this->rendered(__DIR__ . "/../../core/components/successModal.html", array(
                "__MESSAGE__" => $this->successMessage,
            ));
        }

        return "";
    }
}
