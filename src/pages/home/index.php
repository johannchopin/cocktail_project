<?php

// INIT COMPONENTS ZONE
require_once __DIR__ . '/../../core/pageComponent.php';
// INIT COMPONENTS ZONE

class HomePage extends PageComponent
{
    protected $recipes;

    public function __construct()
    {
        parent::__construct();

        $this->setFavouriteRecipes();
        $this->setRecipes();
    }

    public function render(array $replacer = array()): string
    {
        return $this->rendered(__DIR__ . "/template.html", array(
            "__BREADCRUMB__" => $this->getBreadcrumbRender(),
            "__RECIPES__"    => $this->recipesRender(),
        ));
    }

    protected function getBreadcrumbRender(): string
    {
        if ($this->isRequestedIngredientUnique()) {
            return $this->rendered(__DIR__ . "/components/breadcrump.html", array(
                "__BREADCRUMP_LIST__" => $this->ingredientBreadcrumpRender()
            ));
        }

        return $this->rendered(__DIR__ . "/components/breadcrump.html", array(
            "__BREADCRUMP_LIST__" => "<li class='breadcrumb-item'><a href='?p=home'>Home</a></li>",
        ));
    }

    protected function ingredientBreadcrumpRender(): string
    {
        $selectedIngredientId = json_decode($_GET["recipe_contain"])[0];

        $ingredientNameResponse = $this->database->get("SELECT nom FROM ingredient WHERE id = :selectedIngredientId", array(
            "selectedIngredientId" => $selectedIngredientId
        ));
        $ingredientName = $ingredientNameResponse[0]["nom"];

        $ingredientChildrenResponse = $this->database->get("SELECT id, nom FROM ingredient WHERE id IN 
        (SELECT id_ingredient FROM a_pour_super_categorie WHERE id_ingredient_parent = :selectedIngredient)", array(
            "selectedIngredient" => $selectedIngredientId
        ));

        $selectedIngredientParentsRender = $this->ingredientParentsRender($selectedIngredientId);

        if (sizeof($ingredientChildrenResponse) <= 0) {
            return  $selectedIngredientParentsRender . "<li class='breadcrumb-item'>{$ingredientName}</li>";
        }

        $selectedIngredientDropdownRender = $this->ingredientDropdownRender($ingredientName, $ingredientChildrenResponse);

        return  $selectedIngredientParentsRender . "<li class='breadcrumb-item'>{$selectedIngredientDropdownRender}</li>";
    }

    protected function ingredientParentsRender(string $ingredientId): string
    {
        $ingredientParentsRender = '';
        $ingredientParents = $this->getIngredientParents($ingredientId);

        foreach ($ingredientParents as $ingredientParent) {
            $ingredientParentsRender .= "<li class='breadcrumb-item'><a href='?recipe_contain=[{$ingredientParent['id']}]'>{$ingredientParent['name']}</a></li>";
        }

        return $ingredientParentsRender;
    }

    protected function getIngredientParents(string $ingredientId, array $ingredientParents = array()): array
    {
        $ingredientParentIdResponse = $this->database->get("SELECT id, nom FROM ingredient WHERE id IN
        (SELECT id_ingredient_parent FROM a_pour_super_categorie WHERE id_ingredient = :ingredientId)", array(
            "ingredientId" =>  $ingredientId,
        ));

        if (sizeof($ingredientParentIdResponse) <= 0) {
            return array_reverse($ingredientParents);
        }

        array_push($ingredientParents, array(
            "name" => $ingredientParentIdResponse[0]["nom"],
            "id"   => $ingredientParentIdResponse[0]["id"]
        ));

        return $this->getIngredientParents($ingredientParentIdResponse[0]["id"], $ingredientParents);
    }

    protected function ingredientDropdownRender(string $ingredientName, array $ingredientChildren): string
    {
        return $this->rendered(__DIR__ . "/components/ingredientDropdown.html", array(
            "__TITLE__" => $ingredientName,
            "__ITEMS__" => $this->ingredientChildrenRender($ingredientChildren),
        ));
    }

    protected function ingredientChildrenRender(array $children): string
    {
        $ingredientChildrenRender = '';
        foreach ($children as $child) {
            $ingredientChildrenRender .=  "<a class='dropdown-item' href='?recipe_contain=[{$child['id']}]'>{$child['nom']}</a>";
        }

        return $ingredientChildrenRender;
    }

    // TODO: Rename
    protected function isRequestedIngredientUnique(): bool
    {
        if (isset($_GET["recipe_contain"])) {
            $requestedIngredients = json_decode($_GET["recipe_contain"]);

            return sizeof($requestedIngredients) === 1;
        }

        return FALSE;
    }

    protected function setRecipes()
    {

        if (isset($_GET["recipe_contain"]) || isset($_GET["recipe_not_contain"])) {
            $this->recipes = array();
            $this->recipes = $this->getMatchingRecipes();
            $this->sortRecipesByScore();
        } else {
            $this->recipes = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette");
        }
    }

    protected function getMatchingRecipes(): array
    {
        if (isset($_GET["recipe_contain"]) && isset($_GET["recipe_not_contain"])) {
            $elmtsToMatchNumber = sizeof(json_decode($_GET["recipe_contain"])) + sizeof(json_decode($_GET["recipe_not_contain"]));

            $matchingRecipesThatContain = $this->getMatchingRecipesThatContain();
            $matchingRecipesThatNotContain = $this->getMatchingRecipesThatNotContain();

            foreach ($matchingRecipesThatContain as &$matchingRecipeThatContain) {
                foreach ($matchingRecipesThatNotContain as $matchingRecipeThatNotContain) {
                    if ($matchingRecipeThatContain["id"] === $matchingRecipeThatNotContain["id"]) {
                        $matchingRecipeThatContain["score"] = $matchingRecipeThatContain["score"] + 1;
                    }
                }
            }

            return $this->getRecipesWithPercentageScore($matchingRecipesThatContain, $elmtsToMatchNumber);
        } elseif (isset($_GET["recipe_contain"])) {
            $elmtsToMatchNumber = sizeof(json_decode($_GET["recipe_contain"]));

            return $this->getRecipesWithPercentageScore($this->getMatchingRecipesThatContain(), $elmtsToMatchNumber);
        } elseif (isset($_GET["recipe_not_contain"])) {
            $elmtsToMatchNumber = sizeof(json_decode($_GET["recipe_not_contain"]));

            return $this->getRecipesWithPercentageScore($this->getMatchingRecipesThatNotContain(), $elmtsToMatchNumber);
        }
    }

    protected function sortRecipesByScore(): void
    {
        usort($this->recipes, array($this, 'cmp'));
    }

    protected function cmp(array $a, array $b)
    {
        return  $b["score"] - $a["score"];
    }

    protected function getMatchingRecipesThatContain(): array
    {
        $matchingRecipesThatContain = array();
        $ingredientsToMatch = json_decode($_GET["recipe_contain"]);

        foreach ($ingredientsToMatch as $ingredientToMatch) {
            $matchingRecipesThatContainCurrentIngredient = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette WHERE id IN
                (SELECT id_recette FROM est_compose WHERE id_ingredient = :ingredientId)", array(
                "ingredientId" => $ingredientToMatch,
            ));

            foreach ($matchingRecipesThatContainCurrentIngredient as $matchingRecipeThatContainCurrentIngredient) {
                if ($this->isRecipeInRecipes($matchingRecipesThatContain, $matchingRecipeThatContainCurrentIngredient)) {
                    foreach ($matchingRecipesThatContain as &$matchingRecipeThatContain) {
                        if ($matchingRecipeThatContain["id"] === $matchingRecipeThatContainCurrentIngredient["id"]) {
                            $matchingRecipeThatContain["score"] = $matchingRecipeThatContain["score"] + 1;
                        }
                    }
                } else {
                    $matchingRecipeThatContainCurrentIngredient["score"] = 1;

                    array_push($matchingRecipesThatContain, $matchingRecipeThatContainCurrentIngredient);
                }
            }
        }

        return $matchingRecipesThatContain;
    }

    protected function getMatchingRecipesThatNotContain(): array
    {
        $matchingRecipesThatNotContain = array();
        $ingredientsToMatch = json_decode($_GET["recipe_not_contain"]);

        foreach ($ingredientsToMatch as $idIngredient) {
            $matchingRecipesThatNotContainCurrentIngredient = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette WHERE id NOT IN
                (SELECT id_recette FROM est_compose WHERE id_ingredient = :ingredientId)", array(
                "ingredientId" => $idIngredient,
            ));

            foreach ($matchingRecipesThatNotContainCurrentIngredient as $matchingRecipeThatNotContainCurrentIngredient) {
                if ($this->isRecipeInRecipes($matchingRecipesThatNotContain, $matchingRecipeThatNotContainCurrentIngredient)) {
                    foreach ($matchingRecipesThatNotContain as &$matchingRecipeThatContain) {
                        if ($matchingRecipeThatContain["id"] === $matchingRecipeThatNotContainCurrentIngredient["id"]) {
                            $matchingRecipeThatContain["score"] = $matchingRecipeThatContain["score"] + 1;
                        }
                    }
                } else {
                    $matchingRecipeThatNotContainCurrentIngredient["score"] = 1;

                    array_push($matchingRecipesThatNotContain, $matchingRecipeThatNotContainCurrentIngredient);
                }
            }
        }

        return $matchingRecipesThatNotContain;
    }

    protected function getRecipesWithPercentageScore(array $recipes, int $elmtsToMatchNumber): array
    {
        foreach ($recipes as &$recipe) {
            $recipe["score"] = ($recipe["score"] / $elmtsToMatchNumber) * 100;
        }

        return $recipes;
    }

    protected function isRecipeInRecipes(array $recipes, array $givenRecipe): bool
    {
        foreach ($recipes as $recipe) {
            if ($recipe["id"] === $givenRecipe["id"]) {
                return TRUE;
            }
        }

        return FALSE;
    }

    protected function getRecipeIngredientsTag(string $recipeName)
    {
        return $this->database->get(
            "SELECT nom FROM ingredient WHERE 
            id IN (SELECT id_ingredient FROM est_compose WHERE 
            id_recette = (SELECT id FROM recette WHERE titre = :recipeName))",
            array(
                "recipeName" => $recipeName,
            )
        );
    }

    protected function recipeIngredientsTagRender(string $recipeName): string
    {
        $render = "";
        $recipeIngredientsTag = $this->getRecipeIngredientsTag($recipeName);

        foreach ($recipeIngredientsTag as $ingredientName) {
            $ingredientIdResponse = $this->database->get(
                "SELECT id FROM ingredient WHERE nom = :ingredientName ",
                array(
                    "ingredientName" => $ingredientName["nom"],
                )
            );
            $ingredientId = $ingredientIdResponse[0]["id"];

            $badgeRender = $this->rendered(__DIR__ . '/../../core/components/badge.html', array(
                "__BADGE_TYPE__" => "dark",
                "__CONTENT__"    => $ingredientName["nom"],
            ));

            $render .= $this->linkRender("?recipe_contain=[{$ingredientId}]", $badgeRender);
        }

        return $render;
    }

    protected function recipesRender(): string
    {
        $render = "";

        foreach ($this->recipes as $recipe) {
            $render .= $this->rendered(__DIR__ . '/components/recipe.html', array(
                "__COCKTAIL_ID__"   => $recipe["id"],
                "__COCKTAIL_NAME__" => $recipe["titre"],
                "__TAGS__"          => $this->recipeIngredientsTagRender($recipe["titre"]),
                "__SCORE__"         => $this->recipeScoreRender($recipe),
            ));
        }

        return $render;
    }

    protected function recipeScoreRender(array $recipe): string
    {
        if (isset($recipe["score"])) {
            return $this->rendered(__DIR__ . '/components/recipeCardFooter.html', array(
                "__SCORE__" => $recipe["score"]
            ));
        }

        return "";
    }

    protected function setFavouriteRecipes()
    {
        if ($this->isUserConnected()) {
            $favouriteRecipesInDb = $this->getUserFavouriteRecipesFromDb();
            $favouriteRecipesInCookie = $this->getFavouriteRecipesFromCookie();

            $favouriteRecipes = array_unique(array_merge($favouriteRecipesInDb, $favouriteRecipesInCookie));

            $this->updateCookie("favourite_recipes", json_encode($favouriteRecipes));
        }
    }

    protected function getUserFavouriteRecipesFromDb(): array
    {
        $favouriteRecipeResponse = $this->database->get("SELECT id_recette FROM a_pour_recette_favorite WHERE id_client = :userId", array(
            "userId" => $this->getUserId(),
        ));

        if ($favouriteRecipeResponse === array()) {
            return $favouriteRecipeResponse;
        }

        return array_column($favouriteRecipeResponse, "id_recette");
    }
}
