function ApiHandler() {

    this.postRequest = function (link, request, callbackFunction, dataType = 'JSON') {
        $.ajax({
            type: 'POST',
            url: link,
            data: request,
            dataType: dataType,
            success: (response) => {
                callbackFunction(response);
            },
            error: () => {
                alert("Impossible to communicate with the server !");
            }
        });
    }

}
