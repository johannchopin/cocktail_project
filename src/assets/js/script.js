var api = new ApiHandler();
api.postRequest('./core/api/getIngredients.php', {}, function (ingredients) {
    initSearchbarAutocomplete(ingredients);
});

function initSearchbarAutocomplete(ingredients) {
    autocomplete(document.getElementById("searchBarInput"), ingredients);
}

function getCookie(cookieName) {
    var name = "CP_" + cookieName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }

        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }

    return undefined;
}

function setCookie(cookieName, cookieValue, expirationDays = 1000) {
    const d = new Date();
    d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = "CP_" + cookieName + "=" + cookieValue + ";" + expires + ";path=/";
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, generatedSelected, i = this.value;
        var val = (this.value.split(' ')).slice(-1)[0];

        var valFirstLetter = val.charAt(0);
        if (valFirstLetter === '"' || valFirstLetter === '+' || valFirstLetter === '-') {
            val = val.substring(1, val.length);
        }

        var valFirstLetter = val.charAt(0);
        if (valFirstLetter === '"') {
            val = val.substring(1, val.length);
        }


        var elmtToKeep = this.value.substring(0, this.value.length - val.length);
        //debugger;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            var matchingStart = arr[i].substr(0, val.length);
            /*check if the item starts with the same letters as the text field value:*/
            if (matchingStart.toUpperCase() === val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                generatedSelected = document.createElement("DIV");
                /*make the matching letters bold:*/
                generatedSelected.innerHTML = "<strong>" + elmtToKeep + matchingStart + "</strong>";
                generatedSelected.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                generatedSelected.innerHTML += "<input type='hidden' value='" + elmtToKeep + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                generatedSelected.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });

                a.appendChild(generatedSelected);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function getUserToken() {
    return getCookie("token");
}

function toogleFavouriteRecipe(recipeId, isUserConnected) {
    recipeId = String(recipeId);
    var favouriteRecipesString = getCookie("favourite_recipes");

    if (favouriteRecipesString !== undefined) {
        var favouriteRecipes = JSON.parse(favouriteRecipesString);

        if (favouriteRecipes.indexOf(recipeId) !== -1) {
            unsetFavouriteRecipe(recipeId, favouriteRecipes, isUserConnected);
        } else {
            setFavouriteRecipe(recipeId, favouriteRecipes, isUserConnected);
        }
    } else {
        setCookie("favourite_recipes", `["${recipeId}"]`);
    }

    setTimeout(function () {
        location.reload();
    }, 50);
}

function setFavouriteRecipe(recipeId, recipes, isUserConnected) {
    recipes.push(recipeId);
    setCookie("favourite_recipes", JSON.stringify(recipes));

    if (isUserConnected) {
        api.postRequest('./core/api/favouriteRecipe.php', {
            id: recipeId,
            method: "ADD",
            user_token: getUserToken(),
        });
    }
}

function unsetFavouriteRecipe(recipeId, recipes, isUserConnected) {
    var recipeIdIndex = recipes.indexOf(recipeId);
    recipes.splice(recipeIdIndex, 1);
    setCookie("favourite_recipes", JSON.stringify(recipes));

    if (isUserConnected) {
        api.postRequest('./core/api/favouriteRecipe.php', {
            id: recipeId,
            method: "DEL",
            user_token: getUserToken(),
        });
    }
}